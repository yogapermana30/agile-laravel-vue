<?php

namespace App\Models;

use App\Models\UserStory;
use Illuminate\Database\Eloquent\Model;

class SprintDetail extends Model
{
    protected $table = "sprint_details";

    public function userStory()
    {
    	$data = UserStory::find($this->user_story_id)->user_story;
    	return $data;
    }
}
