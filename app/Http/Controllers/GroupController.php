<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Project;
use App\Models\ProjectDetail;
use Illuminate\Http\Request;

class GroupController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($userId)
    {
    	$data = Group::where('user_id', $userId)->get();

        foreach ($data as $key => &$value) {
            $detail = ProjectDetail::where('user_id', $userId)->where('group_id', $value->id)->get();
                    
            $projects = array();            
            foreach ($detail as $key => &$value2) {
                $project = Project::select('id','project_name','description')->find($value2->project_id);                
                array_push($projects,$project);
            }
            $value->projects = $projects;
            $value->project_count = $detail->count();
        }

    	return $this->response->success($data);
    }

    public function store($userId, Request $req)
    {
    	$data = new Group();
    	$data->group_name = $req->group_name;
    	$data->description = $req->description;
    	$data->user_id = $userId;
    	$data->save();

    	return $this->response->created($data);
    }

    public function update($groupId, Request $req)
    {
        $group = Group::find($groupId);

        $group->group_name = $req->group_name;
        $group->description = $req->description;

        $group->update();

        $detail = ProjectDetail::where('group_id', $group->id)->get();
                    
        $projects = array();            
        foreach ($detail as $key => &$value2) {
            $project = Project::select('id','project_name','description')->find($value2->project_id);                
            array_push($projects,$project);
        }
        $group->projects = $projects;
        $group->project_count = $detail->count();

        return $this->response->success($group);
    }

    public function delete($userId, $groupId)
    {
        $group = Group::find($groupId);
        $group->delete();

        return $this->response->deleted($group);
    }
}
