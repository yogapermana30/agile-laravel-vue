<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\SprintDetail;
use App\Models\UserStoryDetail;
use App\Sprint;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SprintController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($projectId)
    {
    	$data = Sprint::where('project_id', $projectId);

        if (isset($_GET['type']) && $_GET['type'] == 'latest') {
            $response = [];

            $data = $data->orderBy('created_at', 'desc')->first();

            if (isset($data)) {
                $userstoryTodo = SprintDetail::where('sprint_id', $data->id)->where('status', '0')->get()->count();
                $userstoryDoing = SprintDetail::where('sprint_id', $data->id)->where('status', '1')->get()->count();
                $userstoryDone = SprintDetail::where('sprint_id', $data->id)->where('status', '2')->get()->count();
                $total = $userstoryTodo + $userstoryDoing + $userstoryDone;

                $response['user_stories']['todo'] = $userstoryTodo;
                $response['user_stories']['doing'] = $userstoryDoing;
                $response['user_stories']['done'] =$userstoryDone;

                return $this->response->success($response);
            }
        } else {
            $data = $data->get();

            foreach ($data as $key => &$value) {
                $start_at = Carbon::parse($value->start_at);
                $end_at = Carbon::parse($value->end_at);
                if ($start_at->month == $end_at->month) {
                    $value->date = $start_at->day." Until ".$end_at->day." ".$start_at->format('M')." ".$start_at->year;    
                } else {
                    $value->date = $start_at->day." ".$start_at->format('M')." - ".$end_at->day." ".$end_at->format('M')." ".$start_at->year;
                }
                $value->day = $start_at->diffInDays($end_at);
                $value->userstory_total = SprintDetail::where('sprint_id', $value->id)->get()->count();
                $value->userstory_done = SprintDetail::where('sprint_id', $value->id)->where('status', '2')->get()->count();
            }

            return $this->response->success($data);
        }        
    }

    public function store($projectId, Request $req)
    {
        $data = new Sprint();
        $data->project_id = $projectId;
        $data->sprint_name = $req->sprint_name;
        $data->sprint_description = $req->sprint_description;
        $data->start_at = Carbon::parse($req->start)->format('Y-m-d');
        $data->end_at = Carbon::parse($req->end)->format('Y-m-d');
        $data->save();

        $start_at = Carbon::parse($data->start_at);
        $end_at = Carbon::parse($data->end_at);
        if ($start_at->month == $end_at->month) {
            $data->date = $start_at->day." Until ".$end_at->day." ".$start_at->format('M')." ".$start_at->year;
        } else {
            $data->date = $start_at->day." ".$start_at->format('M')." Until ".$end_at->day." ".$end_at->format('M')." ".$start_at->year; 
        }
        $data->day = $start_at->diffInDays($end_at);
        $data->userstory_total = SprintDetail::where('sprint_id', $data->id)->get()->count();
        $data->userstory_done = SprintDetail::where('sprint_id', $data->id)->where('status', '2')->get()->count();

        //add Added sprint activities
        $this->activity($data->project_id, Auth::id(), "Added", "Sprint");

        return $this->response->created($data);
    }

    public function update($sprintId, Request $req)
    {
        $sprint = Sprint::find($sprintId);

        $sprint->sprint_name = $req->sprint_name;
        $sprint->sprint_description = $req->sprint_description;
        $sprint->update();

        //add Added sprint activities
        $this->activity($sprint->project_id, Auth::id(), "Updated", "Sprint");

        return $this->response->success("success");
    }


    public function delete($sprintId)
    {
        $sprint = Sprint::find($sprintId);
        $sprint->delete();

        //add Added sprint activities
        $this->activity($sprint->project_id, Auth::id(), "deleted", "Sprint");

        return $this->response->success("success");
    }

    public function activity($projectId, $userId, $action, $type)
    {
        $data = new Activity();
        $data->project_id = $projectId;
        $data->user_id = $userId;
        $data->action = $action;
        $data->type = $type;
        $data->save();
    }
}
