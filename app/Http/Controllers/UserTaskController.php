<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class UserTaskController extends Controller
{
    private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($userId)
    {
    	$data = Task::where('assigned_id', $userId)->get();

    	if (isset($_GET['type']) && $_GET['type']=='count') {
    		return $this->response->success($data->count());
    	}
    	return $this->response->success($data);
    }
}
