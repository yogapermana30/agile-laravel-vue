<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Label;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LabelController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($projectId)
    {
    	$data = Label::where('project_id', $projectId)
    		->get();

    	return $this->response->success($data);
    }

    public function store($projectId, Request $req)
    {
        $data = new Label();
        $data->label_name = $req->label_name;
        $data->label_color = $req->label_color;
        $data->project_id = $projectId;
        $data->save();       

        //add Added user story activities
        $this->activity($data->project_id, Auth::id(), "Added", "Label");

        return $this->response->created($data);
    }

    public function delete($labelId)
    {
        $label = Label::find($labelId);
        $label->delete();
         //add Added user story activities
        $this->activity($label->project_id, Auth::id(), "Deleted", "Label");

        return $this->response->success("Success");
    }

    public function activity($projectId, $userId, $action, $type)
    {
        $data = new Activity();
        $data->project_id = $projectId;
        $data->user_id = $userId;
        $data->action = $action;
        $data->type = $type;
        $data->save();
    }
}
