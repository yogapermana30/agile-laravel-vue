<?php

namespace App\Http\Controllers;

class BaseResponse
{
    //200 – OK – Eyerything is working
    public function success($data, $message = "success")
    {
        $res['code'] = 201;
        $res['message'] = $message;
        $res['data'] = $data;
        return response()->json($res, 200);
    }

    //201 – OK – New resource has been created
    public function created($data)
    {
        $res['code'] = 201;
        $res['status'] = "success";
        $res['data'] = $data;
        return response()->json($res, 201);
    }

    //204 – OK – The resource was successfully deleted
    public function deleted($data)
    {
        $res['code'] = 204;
        $res['status'] = "success";
        $res['data'] = null;
        return response()->json($res, 204);
    }

    //304 – Not Modified – The client can use cached data
    public function notModified($data)
    {
        return response()->json($data, 304);
    }

    /*400 – Bad Request – The request was invalid or cannot be served.
    The exact error should be explained in the error payload. E.g. „The JSON is not valid“*/

    //401 – Unauthorized – The request requires an user authentication
    public function unauthorized($data)
    {
        return response()->json($data, 401);
    }

    //403 – Forbidden – The server understood the request, but is refusing it or the access is not allowed.
    public function forbidden($data)
    {
        return response()->json($data, 403);
    }

    //404 – Not found – There is no resource behind the URI.
    public function notFound($data)
    {
        $res['code'] = 404;
        $res['message'] = "Data is not found";
        $res['data'] = $data;
        return response()->json($res, 404);
    }

    /*422 – Unprocessable Entity – Should be used if the server cannot process the enitity, e.g. if an image cannot be formatted or mandatory fields are missing in the payload.*/

    /*500 – Internal Server Error – API developers should avoid this error. If an error occurs in the global catch blog, the stracktrace should be logged and not returned as response.*/
}
