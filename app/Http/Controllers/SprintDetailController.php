<?php

namespace App\Http\Controllers;

use App\Models\SprintDetail;
use App\Models\UserStory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SprintDetailController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($sprintId)
    {
    	$status = Input::get('status', '0');
    	$data = SprintDetail::where('sprint_id', $sprintId)
    		->where('status', $status)
    		->get();

    	foreach ($data as $key => &$value) {
    		$value->user_story = $value->userStory(); 
    	}
    	return $this->response->success($data);
    }

    public function store($sprintId, Request $req){
        $response = [];

        for ($i=0; $i < $req->length; $i++) { 
            $data = new SprintDetail();
            $data->sprint_id = $sprintId;
            $data->user_story_id = $req->params[$i]['id'];
            $data->save();

            $data->user_story = $data->userStory();
            array_push($response, $data);

            $tmp = UserStory::find($req->params[$i]['id']);
            $tmp->is_used = '1';
            $tmp->update();
        }

        return $this->response->created($response);
    }

    public function update($sprintId, Request $req)
    {
    	foreach ($req->todo as $key => $value) {
    		$data = SprintDetail::find($value['id']);
    		$data->status = "0";
    		$data->update();
    	}
    	foreach ($req->doing as $key => $value) {
    		$data = SprintDetail::find($value['id']);
    		$data->status = "1";
    		$data->update();
    	}
    	foreach ($req->done as $key => $value) {
    		$data = SprintDetail::find($value['id']);
    		$data->status = "2";
    		$data->update();
    	}
    	return $this->response->success(null, "Success updated Data");
    }
}
