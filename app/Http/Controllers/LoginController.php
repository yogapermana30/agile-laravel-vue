<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function auth(Request $req)
    {
		$req->validate([
		    'username' => 'required',
		    'password' => 'required',
		]);

    	$username = $req['username'];
    	$password = $req['password'];

    	if (Auth::attempt(['username' => $username, 'password' => $password]) || Auth::attempt(['email' => $username, 'password' => $password])) {
    		$data = User::find(Auth::id());
    		return $this->response->success($data);
    	} else {
    		return $this->response->success("Data not found", "fail");
    	}
    }

    public function check()
    {
        $data['data'] = Auth::user();
        $data['message'] = "success";
        if (!Auth::check()) {
            $data['data'] = "Fail";
            $data['message'] = "Fail";
        }
    	return $this->response->success($data['data'], $data['message']);
    }

    public function logout()
    {
        Auth::logout();

        return $this->response->success("success");
    }
}
