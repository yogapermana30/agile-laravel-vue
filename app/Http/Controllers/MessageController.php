<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectDetail;
use App\Models\SprintDetail;
use App\Models\UserStory;
use App\Sprint;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($userId)
    {
    	$response = [];

    	$projects = ProjectDetail::where('user_id', $userId)->get();
    	$now = Carbon::now();

    	foreach ($projects as $key => $value) {
    		$sprint = Sprint::where('project_id', $value->project_id)->orderBy('end_at', 'desc')->first();

            if (!isset($sprint)) {
                # code...
            } else {
                $end = Carbon::parse($sprint->end_at);
                if ($end >= $now) {
                    $project_name = Project::find($value->project_id)->project_name;                

                    $userstories = SprintDetail::where('sprint_id', $sprint->id)->get();
                    
                    $message = "You have " . $userstories->count() . " Active User Story in Project " . $project_name;
                    $detail = $now->diffInDays($end)." days remaining.";
                    $data = (object) ['message' => $message, 'details' => $detail];
                    array_push($response, $data);
                }
            }
    	}

    	return $this->response->success($response);
    }

    public function projectGet($projectId)
    {
        $response = [];

        $projects = Project::find($projectId);
        $now = Carbon::now();
        
        $sprint = Sprint::where('project_id', $projectId)->orderBy('end_at', 'desc')->first();

        if (isset($sprint)) {            
            $end = Carbon::parse($sprint->end_at);

            if ($end >= $now) {
                $project_name = Project::find($projectId)->project_name;                

                $userstories = SprintDetail::where('sprint_id', $sprint->id)->where('status','!=','2')->get();
                
                $message = "You have " . $userstories->count() . " Active User Story in Project " . $project_name;
                $detail = $now->diffInDays($end)." days remaining.";
                $data = (object) ['message' => $message, 'details' => $detail];
                array_push($response, $data);
            }
        }
        return $this->response->success($response);
    }
}
