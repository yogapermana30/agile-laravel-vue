<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\UserStory;
use Illuminate\Http\Request;

class TaskController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($usertoryId)
    {
    	$data = Task::where('user_story_id', $usertoryId)->get();

    	return $this->response->success($data);
    }

    public function projectGet($projectId)
    {
        $userstories = UserStory::where('project_id', $projectId)->get();
        $total = 0;

        foreach ($userstories as $key => $value) {
            $tasks = Task::where('user_story_id', $value->id)->get();
            $total += $tasks->count();
        }

        if (isset($_GET['type']) && $_GET['type']=='count') {
            return $this->response->success($total);
        }

        return $this->response->success($data);
    }

    public function store($userstoryId, Request $req)
    {
    	$data = new Task();
    	$data->user_story_id = $userstoryId;
    	$data->task = $req->task;
        $data->isDone = '0';
        $data->assigned_id = null;
    	$data->save();

    	return $this->response->created($data);
    }

    public function update($taskId, Request $req)
    {
        $task = Task::find($taskId);

        if (isset($req->isDone)) {
            $task->isDone = $req->isDone;
        }

        if (isset($req->assigned_id)) {
            if ($req->assigned_id == 0) {
                $task->assigned_id = null;
            } else {
                $task->assigned_id = $req->assigned_id;
            }
        }

        $task->update();

        return $this->response->success($task);
    }

    public function delete($taskId)
    {
        $task = Task::find($taskId);
        $task->delete();

        return $this->response->success("Success");
    }
}
