<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\SprintDetail;
use App\Models\UserStory;
use App\Sprint;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }
 
    public function reportSprint($projectId)
    {
    	$project = Project::find($projectId);
    	$latestSprint = Sprint::where('project_id', $projectId)->orderBy('created_at', 'desc')->first();

    	if (isset($latestSprint)) {    		
    		$totalUserstory = SprintDetail::where('sprint_id', $latestSprint->id)->get()->count();
    		$start = Carbon::parse($latestSprint->start_at);
    		$end = Carbon::parse($latestSprint->end_at);            
    		$spareTime = $start->diffInDays($end);
    		for ($i=0; $i < $spareTime; $i++) { 
    			$response['sprint'][$i]['y'] = Carbon::parse($latestSprint->start_at)->addDays($i)->format('Y-m-d');
    			$response['sprint'][$i]['a'] = $totalUserstory - ($totalUserstory/$spareTime) * ($i);
                $progress = SprintDetail::where('sprint_id', $latestSprint->id)
                    ->where('status', '2')
                    ->whereDate('updated_at','<=', $response['sprint'][$i]['y'])
                    ->get()->count();
                
                    $response['sprint'][$i]['b'] = $totalUserstory - $progress;                
    		}
    	}
    	
    	$todo = 0;
    	$doing = 0;
    	$done = 0;

    	$userstories = UserStory::where('project_id', $projectId)->get();
    	if (isset($userstories)) {    		
    		foreach ($userstories as $key => &$userstory) {
    			$status = SprintDetail::where('user_story_id', $userstory->id)->first();

    			if (isset($status)) {
    				$userstory->status = $status->status;
    				if ($status->status == '0') {
    					$todo += 1;
    				}
    				if ($status->status == '1') {
    					$doing += 1;
    				}
    				if ($status->status == '2') {
    					$done += 1;
    				}
    			} else {
    				$userstory->status = "0";
    				$todo += 1;
    			}
    		}
    		if ($userstories->count() != 0) {
    			$response['progress'] = $done/$userstories->count() * 100;
    		}
    		$response['usData']['todo'] = $todo;
    		$response['usData']['doing'] = $doing;
    		$response['usData']['done'] = $done;
    	}

    	return $this->response->success($response);
    }
}
