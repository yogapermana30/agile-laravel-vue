<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($userId)
    {
    	$tmp = ProjectDetail::select('id', 'user_id', 'project_id', 'group_id', 'role')
    		->where('user_id', $userId)
    		->get();

        if (isset($_GET['type']) && $_GET['type'] == 'count') {
            //for get count
            return $this->response->success($tmp->count());
            
        } else {
            foreach ($tmp as $key => &$value) {
                $value->project = Project::select('project_name', 'description')->find($value->project_id);
                $value->project->member = ProjectDetail::select(DB::raw('count(user_id) as member'))->where('project_id', $value->project_id)->count('user_id');
            }
        }

    	return $this->response->success($tmp);
    }

    public function getProject($project_id)
    {
        $data = Project::select('project_name', 'description')->find($project_id);
        $member = ProjectDetail::select(DB::raw('count(user_id) as member'))->where('project_id', $project_id)->count('user_id');

        $res = ProjectDetail::select('id', 'user_id', 'project_id', 'group_id', 'role')
            ->where('project_id', $project_id)
            ->first();

        $res->project = $data;
        $res->project->member = $member;

        return $res;
    }

    public function store($userId, Request $req)
    {
        $req->validate([
            'project_name' => 'required',
            'description' => 'required',
        ]);

        $data = new Project();
        $data->project_name = $req->project_name;
        $data->description = $req->description;
        $data->save();

        $detail = new ProjectDetail();
        $detail->project_id = $data->id;
        $detail->user_id = $userId;
        $detail->group_id = $req->group_id;
        $detail->save();

        $res = $this->getProject($data->id);

        return $this->response->created($res);
    }

    public function update($projectId, Request $req)
    {
        $projectb = Project::find($projectId);
        $project = ProjectDetail::where('user_id', Auth::user()->id)->where('project_id', $projectId)->first();
        
        if (isset($req->group_id) || $req->group_id == null) {
            $project->group_id = $req->group_id;
        }

        if (isset($req->project_name)) {
            $projectb->project_name = $req->project_name;
            $projectb->description = $req->description;
        }

        $project->update();
        $projectb->update();

        $res = $this->getProject($projectId);

        return $this->response->success($res);
    }

    public function delete($projectId)
    {
        $project = Project::find($projectId);
        $project->delete();

        return $this->response->success("success");
    }
}
