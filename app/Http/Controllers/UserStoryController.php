<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Label;
use App\Models\UserStory;
use App\Models\UserStoryDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserStoryController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($projectId)
    {
    	$data = UserStory::select('id', 'project_id', 'user_story', 'description', 'number_user_story', 'priority', 'is_used')
    		->where('project_id', $projectId)
    		->get();

        if (isset($_GET['type']) && $_GET['type']=='count') {
            return $this->response->success($data->count());
        }

    	foreach ($data as $key => &$value) {
    		$value->labels = UserStoryDetail::select('id', 'label_id')
    			->where('user_story_id', $value->id)
    			->get();
    		foreach ($value->labels as $key => &$value2) {
    			$value2->label_name = Label::select('label_name')
    				->find($value2->label_id)->label_name;

    			$value2->label_color = Label::select('label_color')
    				->find($value2->label_id)->label_color;
    		}
    	}

    	return $this->response->success($data);
    }

    public function store($projectId, Request $req)
    {
        $label_data = explode(',',$req->labels);
    
        $data = new UserStory();
        $data->project_id = $projectId;
        $data->user_id = $req->user_id;
        $data->user_story = $req->user_story;
        $data->description = $req->description;
        $data->priority = $req->priority;
        $data->number_user_story = UserStory::where('project_id', $projectId)->get()->count() + 1;
        $data->save();

        if ($label_data[0] != null) {                
            for ($i=0; $i < count($label_data) ; $i++) { 
                $label = new UserStoryDetail();
                $label->user_story_id = $data->id;
                $label->label_id = $label_data[$i];
                $label->save();
            }
        }

        $data->labels = UserStoryDetail::select('id', 'label_id')
                ->where('user_story_id', $data->id)
                ->get();

        foreach ($data->labels as $key => &$value) {
            $value->label_name = Label::select('label_name')
                ->find($value->label_id)->label_name;

            $value->label_color = Label::select('label_color')
                ->find($value->label_id)->label_color;
        }

        //add Added user story activities
        $this->activity($data->project_id, Auth::id(), "Added", "User Story");

        return $this->response->created($data);
    }

    public function update($userstoryId, Request $req)
    {
        $data = UserStory::find($userstoryId);
        

        if (isset($req->user_story)) {
            $data->user_story = $req->user_story;
        }

        if (isset($req->description)) {
            $data->description = $req->description;
        }

        if (isset($req->labels)) {
            if ($req->labels['action'] == "delete") {
                UserStoryDetail::where('user_story_id', $userstoryId)
                    ->where('label_id', $req->labels['label_id'])
                    ->first()
                    ->delete();
            } else {
                $label = new UserStoryDetail();
                $label->user_story_id = $userstoryId;
                $label->label_id = $req->labels['label_id'];
                $label->save();
            }
        }

        if (isset($req->priority)) {
            $data->priority = $req->priority;
        }

        $data->update();

        $data->labels = UserStoryDetail::select('id', 'label_id')
                ->where('user_story_id', $data->id)
                ->get();

        foreach ($data->labels as $key => &$value) {
            $value->label_name = Label::select('label_name')
                ->find($value->label_id)->label_name;

            $value->label_color = Label::select('label_color')
                ->find($value->label_id)->label_color;
        }

        //add update user story activities
        $this->activity($data->project_id, Auth::id(), "Updated", "User Story");

        return $this->response->success($data);
    }

    public function delete($userStoryId)
    {
        $data = UserStory::find($userStoryId);

        //add delete user story activities 
        $this->activity($data->project_id, Auth::id(), "Deleted", "User Story");

        $data->delete();

        return $this->response->deleted($data);
    }

    public function activity($projectId, $userId, $action, $type)
    {
        $data = new Activity();
        $data->project_id = $projectId;
        $data->user_id = $userId;
        $data->action = $action;
        $data->type = $type;
        $data->save();
    }
}
