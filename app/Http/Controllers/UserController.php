<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
	private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function store(Request $req)
    {
    	$req->validate([
    	    'name' => 'required',
    	    'username' => 'required|unique:users,username',
    	    'email' => 'required|email|unique:users,email',
    	    'password' => 'required',
    	]);

    	$data = new User();
    	$data->name = $req->name;
    	$data->username = $req->username;
    	$data->email = $req->email;
    	$data->password = bcrypt($req->password);
    	$data->save();

    	Auth::attempt(['email' => $req->email, 'password' => $req->password]);

    	return $this->response->created($data);
    }

    public function update($userId, Request $req)
    {
        $user = User::find($userId);

        $req->validate([
            'username' => 'unique:users,username,'.$userId,
            'email' => 'email|unique:users,email,'.$userId,
        ]);

        if (isset($user->name)) {
            $user->name = $req->name;
        }

        if (isset($user->username)) {
            $user->username = $req->username;
        }

        if (isset($user->email)) {
            $user->email = $req->email;
        }

        $user->update();

        return $this->response->success($user);
    }

    public function changePassword(Request $req)
    {
        $user = User::find($req->user_id);

        if (Hash::check($req->old_password, $user->password)) {
            $user->password = bcrypt($req->new_password);
            $user->update();
            return $this->response->success($user);
        } else {
            return $this->response->notFound("fail");
        }
    }
}
