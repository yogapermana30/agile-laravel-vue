<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Project;
use App\Models\ProjectDetail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($projectId)
    {
    	$data = Activity::where('project_id', $projectId)->orderBy('created_at', 'desc')->get();

    	foreach ($data as $key => $value) {
    		$value->user_name = User::find($value->user_id)->name;
    		$value->project_name = Project::find($projectId)->project_name;

    		$now = Carbon::now();
    		$time = $value->created_at;

	        if ($time->diffInSeconds($now) < 60) {
	            $value->activity_time = $time->diffInSeconds($now)." seconds";
	        } else
	        if ($time->diffInMinutes($now) < 60) {
	            $value->activity_time = $time->diffInMinutes($now)." minutes";
	        } else 
	        if ($time->diffInHours($now) < 24){
	            $value->activity_time = $time->diffInHours($now)." hours";
	        } else 
	        if ($time->diffInDays($now) < 31){
	            $value->activity_time = $time->diffInDays($now)." days";
	        }
    	}

    	return $this->response->success($data);
    }

    public function userGet($userId)
    {
        $projects = ProjectDetail::where('user_id', $userId)->get();
        $response = [];

        foreach ($projects as $key => $value) {
            $activities = Activity::where('project_id', $value->project_id)->orderBy('created_at', 'desc')->get();
            

            foreach ($activities as $key => &$value) {
                $value->user_name = User::find($value->user_id)->name;
                $value->project_name = Project::find($value->project_id)->project_name;

                $now = Carbon::now();
                $time = $value->created_at;

                if ($time->diffInSeconds($now) < 60) {
                    $value->activity_time = $time->diffInSeconds($now)." seconds";
                } else
                if ($time->diffInMinutes($now) < 60) {
                    $value->activity_time = $time->diffInMinutes($now)." minutes";
                } else 
                if ($time->diffInHours($now) < 24){
                    $value->activity_time = $time->diffInHours($now)." hours";
                } else 
                if ($time->diffInDays($now) < 31){
                    $value->activity_time = $time->diffInDays($now)." days";
                }

                array_push($response, $value);
            }
        }

        return $this->response->success($response);
    }
}
