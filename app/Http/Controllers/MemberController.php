<?php

namespace App\Http\Controllers;

use App\Models\ProjectDetail;
use App\User;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($projectId)
    {
    	$projects = ProjectDetail::where('project_id', $projectId)->get();
    	if (isset($_GET['type']) && $_GET['type'] == 'count') {
    		$response['count'] = $projects->count();
    		return $this->response->success($response['count']);
    	}

        foreach ($projects as $key => &$value) {
            $value->name = User::find($value->user_id)->name;
        }

    	return $this->response->success($projects);
    }

    public function store($projectId, Request $req)
    {
        $user = User::where('username', $req->username)->first();

        if(isset($user)) {
            $check = ProjectDetail::where('user_id', $user->id)->where('project_id', $projectId)->first();
            if (isset($check)) {                
                return $this->response->success("User has been added in this project!", "Fail");
            } else {                
                $member = new ProjectDetail();
                $member->project_id = $projectId;
                $member->user_id = $user->id;
                $member->group_id= null;
                $member->role = $req->role;
                $member->save();

                return $this->response->created("success");
            }
        } else {
            $user = User::where('email', $req->username)->first();
            if (isset($user)) {
                $check = ProjectDetail::where('user_id', $user->id)->where('project_id', $projectId)->first();
                if ($check) {                
                    return $this->response->success("User has been added in this project!", "Fail");
                } else {                
                    $member = new ProjectDetail();
                    $member->project_id = $projectId;
                    $member->user_id = $user->id;
                    $member->group_id= null;
                    $member->role = $req->role;
                    $member->save();

                    return $this->response->created("success");
                }
            } else {
                return $this->response->success("Email or username not valid!", "Fail");
            }
        }        
    }

    public function delete($memberId)
    {
        $member = ProjectDetail::find($memberId);
        $member->delete();

        $this->response->success("success");
    }

    public function update($memberId, Request $req)
    {
        $member = ProjectDetail::find($memberId);
        $member->role = $req->role;
        $member->update();

        $this->response->success($member);
    }
}
