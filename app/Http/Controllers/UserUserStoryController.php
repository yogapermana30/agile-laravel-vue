<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class UserUserStoryController extends Controller
{
    private $response;

    public function __construct()
    {
        $this->response = new BaseResponse();
    }

    public function get($userId)
    {
    	$data = Task::select('user_story_id')->where('assigned_id', $userId)->groupBy('user_story_id')->get();

    	if (isset($_GET['type']) && $_GET['type']=='count') {
    		return $this->response->success($data->count());
    	}

    	return $this->response->success($data);
    }
}
