<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/{any}', 'MasterController@index')->where('any', '^(?!.*api).*$[\/\w\.-]*');

Route::group(['prefix' => 'api/v1'], function(){
    Route::post('login', 'LoginController@auth');
    Route::get('session', 'LoginController@check');
    Route::post('logout', 'LoginController@logout');
    Route::post('changepassword', 'UserController@changePassword');

    Route::group(['prefix' => 'users'], function(){
    	Route::post('/', 'UserController@store');
    	Route::group(['prefix' => '{userId}'], function(){
            Route::put('/', 'UserController@update');
    		Route::get('/groups', 'GroupController@get');
    		Route::post('/groups', 'GroupController@store');
            Route::delete('/groups/{groupId}', 'GroupController@delete');

            Route::get('/projects', 'ProjectController@get');
            Route::post('/projects', 'ProjectController@store');

            Route::get('/tasks', 'UserTaskController@get');

            Route::get('/userstories', 'UserUserStoryController@get');

            Route::get('/messages', 'MessageController@get');

            Route::get('/activities', 'ActivityController@userGet');
    	});
    });

    Route::group(['prefix' => 'projects/{projectId}'], function(){
        Route::put('/', 'ProjectController@update');
        Route::delete('/', 'ProjectController@delete');
        Route::get('/userstories', 'UserStoryController@get');
        Route::post('/userstories', 'UserStoryController@store');
        Route::get('/labels', 'LabelController@get');
        Route::post('/labels', 'LabelController@store');
        Route::get('/sprints', 'SprintController@get');
        Route::post('/sprints', 'SprintController@store');
        Route::get('/activities', 'ActivityController@get');
        Route::get('/members', 'MemberController@get');
        Route::post('/members', 'MemberController@store');
        Route::get('/tasks', 'TaskController@projectGet');
        Route::get('/messages', 'MessageController@projectGet');    
        Route::get('/reports', 'ReportController@reportSprint');
    });

    Route::group(['prefix' => 'userstories/{userstoryId}'], function(){
        Route::put('/', 'UserStoryController@update');        
        Route::delete('/', 'UserStoryController@delete');
        Route::get('tasks', 'TaskController@get');
        Route::post('tasks', 'TaskController@store');
    });

    Route::group(['prefix' => 'sprints/{sprintId}'], function(){
        Route::put('/', 'SprintController@update');
        Route::delete('/', 'SprintController@delete');
        Route::get('sprintbacklog', 'SprintDetailController@get');
        Route::post('sprintbacklog', 'SprintDetailController@store');
        Route::put('sprintbacklog', 'SprintDetailController@update');
    });

    Route::group(['prefix' => 'labels/{labelId}'], function() {
        Route::delete('/', 'LabelController@delete');
    });

    Route::group(['prefix' => 'members/{memberId}'], function() {
        Route::put('/', 'MemberController@update');
        Route::delete('/', 'MemberController@delete');
    });

    Route::group(['prefix' => 'groups/{groupId}'], function() {
        Route::put('/', 'GroupController@update');
    });

    Route::group(['prefix' => 'tasks/{taskId}'], function () {
        Route::put('/', 'TaskController@update');
        Route::delete('/', 'TaskController@delete');
    });
});