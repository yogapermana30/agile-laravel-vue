require('./bootstrap');

window.Vue = require('vue');
import jQuery from 'jquery';
import VueRouter from 'vue-router';
import VueNoty from 'vuejs-noty'
import Raphael from 'raphael/raphael'
global.Raphael = Raphael

Vue.use(VueRouter);
Vue.use(VueNoty, {
	theme: 'relax',
  	progressBar: false,
  	timeout: 3000,
  	layout: 'topRight'
});

import App from './views/App';
import Login from './views/Login';
import Home from './views/Home';
import Profile from './views/Profile';
import Projects from './views/Projects';
import Dashboard from './views/Dashboard';
import UserStories from './views/UserStories';
import Sprints from './views/Sprints';
import SprintDetails from './views/SprintDetails';
import Activity from './views/Activity';
import ActivityProject from './views/ActivityProject';
import Labels from './views/Labels';
import Groups from './views/Groups';
import Notes from './views/Notes';
import Report from './views/Report';
import Setting from './views/Setting';

import store from './store';

const router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/login',
			name: 'login',
			component: Login,
		},
		{
			path: '/home',
			name: 'home',
			component: Home,
		},
		{
			path: '/profile',
			name: 'profile',
			component: Profile,
		},
		{
			path: '/projects',
			name: 'projects',
			component: Projects,
		},
		{
			path: '/activity',
			name: 'activity',
			component: Activity,
		},
		{
			path: '/groups',
			name: 'groups',
			component: Groups,
		},
		{
			path: '/projects/:projectId',
			name: 'dashboard',
			component: Dashboard,
		},
		{
			path: '/projects/:projectId/userstories',
			name: 'user_stories',
			component: UserStories,
		},
		{
			path: '/projects/:projectId/sprints',
			name: 'sprints',
			component: Sprints,
		},
		{
			path: '/projects/:projectId/sprints/:sprintId',
			name: 'sprint_details',
			component: SprintDetails,
		},
		{
			path: '/projects/:projectId/activity',
			name: 'activityProject',
			component: ActivityProject,
		},
		{
			path: '/projects/:projectId/labels',
			name: 'labels',
			component: Labels,
		},
		{
			path: '/projects/:projectId/report',
			name: 'report',
			component: Report,
		},
		{
			path: '/projects/:projectId/setting',
			name: 'setting',
			component: Setting,
		}
	]
});

// check session
router.beforeEach((to, from, next) => {
	axios.get('session').then(response => {
		if (response.data.data == "Fail") {
			if (to.fullPath !== "/login") {
	    		router.push('/login');
			}else{				
				next();
			}
		} else {
			if (to.fullPath == "/login") {
				store.state.user = response.data.data;
	    		router.push('/home');
			} else {
				store.state.user = response.data.data;
	    		next();				
			}
		}
	}).catch(error => {
	    router.push('/login');
	});
})

const app = new Vue({
    el: '#app',
    store,
    components: {App},
    router,
});
