import * as types from './mutation-types';

/*export const changeMenu = ({commit}, index) => {
	commit(types.CHANGE_MENU_STATE, index);
}*/

export const changeMenu = ({commit}, type) => {
	commit(types.INIT_MENU);
	commit(types.CHANGE_MENU, type);
}

export const initUser = ({commit}) => {
	axios.get('session')
		.then(response => {
			if (response.data.data !== "Fail") {
				commit(types.INIT_USER, response.data.data);
			}
		})
}

export const initMessagesUser = ({commit}, userId) => {
	axios.get(`users/${userId}/messages`)
		.then(response => {
			commit(types.INIT_MESSAGES, response.data.data);
		})
}

/*LABELS*/
export const initLabels = ({commit}, projectId) => {
	axios.get('projects/'+projectId+'/labels')
		.then(response => {
			commit(types.INIT_LABELS, response.data.data);
		});
}
export const addLabel = ({commit}, [projectId, form]) => {
	return axios.post('projects/'+projectId+'/labels', form)
		.then(response => {
			commit(types.ADD_LABEL, response.data.data);
			return response;
		});
}

/*TASKS*/
export const initTasks = ({commit}, userstoryId) => {
	return axios.get('userstories/'+userstoryId+'/tasks')
		.then(response => {
			commit(types.INIT_TASKS, response.data.data);
			return response;
		});
}
export const addTask = ({commit}, [userstoryId, form]) => {
	return axios.post('userstories/'+userstoryId+'/tasks', form)
		.then(response => {
			commit(types.ADD_TASK, response.data.data);
			return response;
		});
}

export const initMembers = ({commit}, projectId) => {
	return axios.get(`projects/${projectId}/members`)
		.then(response => {
			commit(types.INIT_MEMBERS, response.data.data);
			return response;
		})
}

export const openModal = ({commit}, id) => {
	commit(types.OPEN_MODAL, id);
}