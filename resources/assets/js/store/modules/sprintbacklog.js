import * as types from '../mutation-types';

const state = {
	todo: [],
	doing: [],
	done: [],
	data:[],
	isLoading:false,
	filter:'',
}

const getters = {
	
}

const actions = {
	addSprintBacklog ({commit}, [sprintId, form]) {
		return axios.post('sprints/'+sprintId+'/sprintbacklog', form)
			.then(response => {
				commit(types.ADD_SPRINTBACKLOG, response.data.data);	
				return response.data.data;			
			});
	},
	initSprintBacklog ({commit}, sprintId)  {
		axios.get('sprints/'+sprintId+'/sprintbacklog?status=0')
			.then(response => {
				commit(types.INIT_SPRINTBACKLOG_TODO, response.data.data);
			});
		axios.get('sprints/'+sprintId+'/sprintbacklog?status=1')
			.then(response => {
				commit(types.INIT_SPRINTBACKLOG_DOING, response.data.data);
			});
		axios.get('sprints/'+sprintId+'/sprintbacklog?status=2')
			.then(response => {
				commit(types.INIT_SPRINTBACKLOG_DONE, response.data.data);
			});
	},
	updateSprintBacklog ({commit}, [sprintId, form])  {
		axios.put('sprints/'+sprintId+'/sprintbacklog', form)
			.then(response => {
				
			});
	}
}

const mutations = {
	[types.ADD_SPRINTBACKLOG] (state, payload) {
		for (var i = 0; i < payload.length; i++) {			
			state.todo.push(payload[i]);
		}
	},
	[types.INIT_SPRINTBACKLOG_TODO] (state, payload) {
		state.todo = (payload);
	},
	[types.INIT_SPRINTBACKLOG_DOING] (state, payload) {
		state.doing = (payload);
	},
	[types.INIT_SPRINTBACKLOG_DONE] (state, payload) {
		state.done = (payload);
	},
	[types.UPDATE_SPRINTBACKLOG_TODO] (state, payload) {
		state.todo = (payload);
	},
	[types.UPDATE_SPRINTBACKLOG_DOING] (state, payload) {
		state.doing = (payload);
	},
	[types.UPDATE_SPRINTBACKLOG_DONE] (state, payload) {
		state.done = (payload);
	},
}

export default {
	state,
	getters,
	actions,
	mutations,
}