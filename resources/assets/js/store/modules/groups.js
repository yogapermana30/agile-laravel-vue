import * as types from '../mutation-types';

const state = {
	data: [],
	is_loading: false,
	filter: '',
}

const getters = {

}

const actions = {
	initGroups ({commit}, userId) {
		axios.get('users/'+userId+'/groups')
			.then(response => {
				commit(types.INIT_GROUPS, response.data.data);
			})
	},
	addGroup ({commit}, [userId, form]) {
		return axios.post('users/'+userId+'/groups', form)
			.then(response => {
				let data = response.data.data;
				data.project_count = 0;
				commit(types.ADD_GROUP, data);
				return response.data.data;
			})
	},
	deleteGroup ({commit}, [userId,groupId]) {
		return axios.delete(`users/${userId}/groups/${groupId}`)
			.then(response => {
				commit(types.DELETE_GROUP, groupId);
				return response;
			});
	},
	updateGroup ({commit}, [groupId, form]) {
		return axios.put(`groups/${groupId}`, form)
			.then(response => {
				commit(types.UPDATE_GROUP, response.data.data);
				return response.data.data;
			})
	}
}

const mutations = {
	[types.INIT_GROUPS] (state, payload) {
		state.data = payload;
	},
	[types.ADD_GROUP] (state, payload) {
		state.data.push(payload);
	},
	[types.DELETE_GROUP] (state, id) {
		for (var i = 0; i < state.data.length; i++) {
			if (state.data[i].id == id) {
				state.data.splice(i, 1);
				break;
			}
		}
	},
	[types.UPDATE_GROUP] (state, payload) {
		//search data from state with the same id and update
		for (var i = 0; i < state.data.length; i++) {
			if (state.data[i].id == payload.id) {
				state.data[i] = payload;
				break;
			}
		}
	}
}

export default {
	state,
	getters,
	actions,
	mutations,
}