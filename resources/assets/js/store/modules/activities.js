import * as types from '../mutation-types';

const state = {
	data: [],
	isLoading: false,
	filter: '',
}

const getters = {

}

const actions = {
	initActivities ({commit}, projectId) {
		axios.get(`projects/${projectId}/activities`)
			.then(response => {
				commit(types.INIT_ACTIVITIES, response.data.data);
			});
	},
	initActivitiesUser ({commit}, userId) {
		axios.get(`users/${userId}/activities`)
			.then(response => {
				commit(types.INIT_ACTIVITIES, response.data.data);
			});
	},
}

const mutations = {
	[types.INIT_ACTIVITIES] (state, payload) {
		state.data = payload;
	}
}

export default {
	state,
	getters,
	actions,
	mutations,
}