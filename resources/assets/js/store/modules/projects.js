import * as types from '../mutation-types';

const state = {
	data: [],
	is_loading: false,
	filter: '',
}

const getters = {
	projects: state => {
		return state.data;
	}
}

const actions = {
	initProjects ({commit}, userId) {
		commit(types.PROJECTS_LOADING, true);
		return axios.get('users/'+userId+'/projects')
			.then(response => {
				commit(types.INIT_PROJECTS, response.data.data);
				commit(types.PROJECTS_LOADING, false);
				return response.data.data;
			})
	},
	addProject ({commit}, [userId, form]) {
		return axios.post('users/'+userId+'/projects', form)
			.then(response => {
				commit(types.ADD_PROJECT, response.data.data);
				return response;
			})
			.catch(error => {
				throw error;
			})
	},
	updateProject ({commit}, [projectId, form]) {
		return axios.put(`projects/${projectId}`, form)
			.then(response => {
				commit(types.UPDATE_PROJECT, response.data.data);
				return response;
			});
	}
}

const mutations = {
	[types.PROJECTS_LOADING] (state, status) {
		state.is_loading=status;
	},
	[types.INIT_PROJECTS] (state, payload) {
		state.data = payload;
	},
	[types.ADD_PROJECT] (state, payload) {
		state.data.push(payload);
	},
	[types.UPDATE_FILTER] (state, status) {
		state.filter = status;
	},
	[types.UPDATE_PROJECT] (state, payload) {
		//search data from state with the same id and update
		for (var i = 0; i < state.data.length; i++) {
			if (state.data[i].id == payload.id) {
				state.data[i] = payload;
				break;
			}
		}
	},
}

export default {
	state,
	getters,
	actions,
	mutations,
}