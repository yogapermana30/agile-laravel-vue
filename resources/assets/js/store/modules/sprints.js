import * as types from '../mutation-types';

const state = {
	data: [],
	isLoading: false,
	filter: '',
}

const getters = {
	sprints: state => {
		return state.data;
	}
}

const actions = {
	initSprints ({commit}, projectId) {
		axios.get('projects/'+projectId+'/sprints')
			.then(response => {
				commit(types.INIT_SPRINTS, response.data.data);
			});
	},
	addSprint ({commit}, [projectId, form]) {
		return axios.post('projects/'+projectId+'/sprints', form)
			.then(response => {
				commit(types.ADD_SPRINT, response.data.data);
				return response.data.data;
			});
	}
}

const mutations = {
	[types.INIT_SPRINTS] (state, payload) {
		state.data = payload;
	},
	[types.ADD_SPRINT] (state, payload) {
		state.data.push(payload);
	},
}

export default {
	state,
	getters,
	actions,
	mutations,
}