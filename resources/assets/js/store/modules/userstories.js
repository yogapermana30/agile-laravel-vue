import * as types from '../mutation-types';

const state = {
	data:[],
	isLoading:false,
	filter:'',
}

const getters = {
	userstories: state => {
		return state.data;
	},
	unUsedUserstories: state => {
		let data = state.data;
		for (var i = 0; i < data.length; i++) {
			data[i].tmp_used = 0;
		}
		return data.filter(unUsed => unUsed.is_used == 0);
	}
}

const actions = {
	/*USERSTORIES*/
	initUserStories ({commit}, projectId) {
		axios.get('projects/'+projectId+'/userstories')
			.then(response => {
				commit(types.INIT_USERSTORIES, response.data.data);
			});
	},
	addUserstory ({commit}, [projectId, form])  {
		return axios.post('projects/'+projectId+'/userstories', form)
			.then(response => {
				commit(types.ADD_USERSTORY, response.data.data);
			});
	},
	updateUserstory ({commit}, [userStoryId, form])  {
		commit(types.USERSTORIES_LOADING, true);
		return axios.put('userstories/'+userStoryId, form)
			.then(response => {
				commit(types.UPDATE_USERSTORY, response.data.data);
				commit(types.USERSTORIES_LOADING, false);
				return response;
			})
	},
	deleteUserstory ({commit}, userStoryId) {
		return axios.delete('userstories/'+userStoryId)
			.then(response => {
				commit(types.DELETE_USERSTORY, userStoryId);
				return response;
			});
	}
}

const mutations = {
	/*USERSTORIES*/
	[types.INIT_USERSTORIES] (state, payload) {
		state.data = payload;
	},
	[types.ADD_USERSTORY] (state, payload) {
		state.data.push(payload);
	},
	[types.UPDATE_USERSTORY] (state, payload) {
		//search data from state with the same id and update
		for (var i = 0; i < state.data.length; i++) {
			if (state.data[i].id == payload.id) {
				state.data[i] = payload;
				break;
			}
		}
	},
	[types.USERSTORIES_LOADING] (state, status) {
		state.isLoading = status;
	},
	[types.DELETE_USERSTORY] (state, id) {
		for (var i = 0; i < state.data.length; i++) {
			if (state.data[i].id == id) {
				state.data.splice(i, 1);
				break;
			}
		}
	}
}

export default {
	state,
	getters,
	actions,
	mutations,
}