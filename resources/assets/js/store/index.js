import Vue from 'vue';
import Vuex from 'vuex';
import { mutations } from './mutations';
import * as actions from './actions';

//modules
import projects from './modules/projects';
import groups from './modules/groups';
import userstories from './modules/userstories';
import sprints from './modules/sprints';
import sprintbacklog from './modules/sprintbacklog';
import activities from './modules/activities';


Vue.use(Vuex);

const state = {
	is_home: false,
	is_projects: false,
	is_activity: false,
	is_groups: false,	
	user:{},
	labels: {
		data:[],
	},
	tasks: {
		data:[],
	},
	sprints: {
		data: [],
	},
	messages: {
		data: [],
	},
	members:[],
}

export default new Vuex.Store({
	state,
	mutations,
	actions,
	modules: {
		projects,
		groups,
		userstories,
		sprints,
		sprintbacklog,
		activities
	}
})