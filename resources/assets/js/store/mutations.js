import * as types from './mutation-types';

export const mutations = {
	[types.INIT_MENU] (state) {
		state.is_home = false;
		state.is_projects = false;
		state.is_activity = false;
		state.is_groups = false;
	},
	[types.CHANGE_MENU] (state, type){
		switch (type) {
			case 'home':
				state.is_home = true;
				break;
			case 'projects':
				state.is_projects = true;
				break;
			case 'activity':
				state.is_activity = true;
				break;
			case 'groups':
				state.is_groups = true;
				break;
			default:
				// statements_def
				break;
		}
	},

	[types.INIT_MESSAGES] (state, payload) {
		state.messages.data = payload;
	},

	[types.INIT_USER] (state, payload) {
		state.user = payload;
	},	

	/*LABELS*/
	[types.INIT_LABELS] (state, payload) {
		state.labels.data = payload;
	},
	[types.ADD_LABEL] (state, payload) {
		state.labels.data.push(payload);
	},

	/*TASKS*/
	[types.INIT_TASKS] (state, payload) {
		state.tasks.data = payload;
	},
	[types.ADD_TASK] (state, payload) {
		state.tasks.data.push(payload);
	},	


	[types.INIT_MEMBERS] (state, payload) {
		state.members=payload;
	},	

	[types.OPEN_MODAL] (state, id) {
		$('#'+id).modal({
			onApprove: function() {
				return false
			}
		}).modal('show');
	},
}